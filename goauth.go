package goauth

import (
	"fmt"
	"sync"
)

// создание переменных для ошибок
var (
	errWrongPassword         = fmt.Errorf("wrong username or password")
	errAuthForbidden         = fmt.Errorf("authorization is forbidden")
	errSessionIsNotCompleted = fmt.Errorf("session is not completed")
	errSession               = fmt.Errorf("session error")
)

// IsWrongPassword возврашает истину если ошибка это неверный логин пароль
func IsWrongPassword(err error) bool { return err == errWrongPassword }

// IsAuthForbidden возвращает истину если ошибка это запрет авторизации
func IsAuthForbidden(err error) bool { return err == errAuthForbidden }

// IsSessionNotCompleted возвращает истину если ошибка это незавершеная сессия
func IsSessionNotCompleted(err error) bool { return err == errSessionIsNotCompleted }

// IsSessionError возвращает истину если ошибка в сесии
func IsSessionError(err error) bool { return err == errSession }

// Config интерфейс для конфигурации пакета
type Config interface {
	GetUser() string
	GetPassword() string
	GetDBName() string
	GetHost() string
	GetPort() int
}

// Logger интерфейс для логирования в пакете
type Logger interface {
	Debug(v ...interface{}) bool
	Info(v ...interface{}) bool
	Warning(v ...interface{}) bool
	Error(v ...interface{}) bool
	Critical(v ...interface{}) bool
	Panic(v ...interface{})
}

var _once sync.Once
var _auth *Auth
var config Config
var log Logger

// SetLogger устанавливает логгер для пакета
func SetLogger(logger Logger) { log = logger }

// SetConfig устанавливает конфиг для пакета
func SetConfig(cnf Config) { config = cnf }

// GetAuth возвращает текущий объект авторизации
// вызовет панику если config и log не были установлены сетерами
func GetAuth() *Auth {
	if log == nil {
		panic("log is nil")
	}
	if config == nil {
		log.Panic("config is nil")
	}
	_once.Do(func() {
		db := &database{}
		db.connect()
		_auth = &Auth{db: db}
	})
	return _auth
}
