package goauth

import (
	"crypto/sha256"
	"fmt"
	"time"
)

func hash(s string) string { return fmt.Sprintf("%x", sha256.Sum256([]byte(s))) }

// Auth структура для работы с аутентификацией
type Auth struct {
	db *database
}

// NewSession создает новую сессию для пользователя
func (a *Auth) NewSession(user *User) (*Session, error) {
	session := &Session{
		UserID: user.ID,
		Token:  hash(user.Login + time.Now().String()),
	}
	session.LastUpdate.Time = time.Now()
	session.LastUpdate.Valid = true
	err := a.db.insertSession(session)
	if err != nil {
		log.Info(user)
		return nil, err
	}
	log.Info("session", session, "create for user", user)
	return session, nil
}

// SaltThePass возвращает просоленый пароль
func (a *Auth) SaltThePass(pass, salt string) string { return hash(pass + salt) }

// CreateSalt создает соль для пользователя по паролю
func (a *Auth) CreateSalt(login string) string { return hash(login + time.Now().String()) }

// Login проверяет пользователя и в случае успеха создает сессию
func (a *Auth) Login(login, pass string) (*User, *Session, error) {
	user, err := a.db.selectUserByLogin(login)
	if err != nil {
		return nil, nil, errWrongPassword
	}
	if a.SaltThePass(pass, user.Salt) != user.Pass {
		log.Info("incorrect password user", user)
		return nil, nil, errWrongPassword
	}
	// находится после проверки пароля для того, чтобы нельзя было узнать про пользователя без знания его пароля
	if user.CanAuth == false {
		return nil, nil, errAuthForbidden
	}
	session, err := a.NewSession(user)
	if err != nil {
		return nil, nil, err
	}
	log.Info(user, "logged in")
	return user, session, nil
}

// Logout завершает сессию
func (a *Auth) Logout(token string) error {
	c, err := a.db.deleteSessionByToken(token)
	if err != nil {
		return err
	}
	if c != 1 {
		log.Error(errSessionIsNotCompleted)
		return errSessionIsNotCompleted
	}
	log.Info("session", token, "completed")
	return nil
}

// GetSession возвращает текущую сессию пользователя или ошибку если сессии не существует
func (a *Auth) GetSession(token string) (*User, *Session, error) {
	session, err := a.db.selectSessionByToken(token)
	if err != nil {
		return nil, nil, errSession
	}
	a.db.updateSessionTime(token, time.Now())
	user, err := a.db.selectUserByID(session.UserID)
	if err != nil {
		return nil, nil, errSession
	}
	return user, session, nil
}

// HasPerm проверяет есть ли у пользователя привелегия
func (a *Auth) HasPerm(user *User, code string) (bool, error) {
	perm, err := a.db.selectPermissionByCode(code)
	if err != nil {
		return false, err
	}
	if (user.PermMask & perm.Mask) != perm.Mask {
		return false, nil
	}
	return true, nil
}

// GetUserByID возвращает пользователя по его ид
func (a *Auth) GetUserByID(id int) (*User, error) {
	return a.db.selectUserByID(id)
}

// GetUsers возврашает всех пользователей
func (a *Auth) GetUsers() ([]*User, error) {
	return a.db.selectUsers()
}

// ChangeUser изменяет данные пользователя если он существует иначе добавляет нового
func (a *Auth) ChangeUser(user *User) (int, error) {
	var id int
	var err error
	if _, err = a.db.selectUserByID(user.ID); err != nil {
		id, err = a.db.insertUser(user)
		if err != nil {
			return -1, err
		}
	} else {
		err = a.db.updateUser(user)
		if err != nil {
			return -1, err
		}
		id = user.ID
	}
	return id, nil
}

// GetPermissions возвращает список прав доступа
func (a *Auth) GetPermissions() ([]*Permission, error) {
	return a.db.selectPermissions()
}
