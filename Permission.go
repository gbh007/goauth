package goauth

import (
	"database/sql"
)

// Permission структура для отображения данных из таблицы уровней доступа
type Permission struct {
	Code string
	Name sql.NullString
	Mask int
}

// selectPermissions возвращает список уровней доступа
func (db *database) selectPermissions() ([]*Permission, error) {
	perms := make([]*Permission, 0)
	rows, err := db.db.Query(`SELECT code, name, mask FROM auth.permissions`)
	if err != nil {
		log.Error(err)
		return nil, err
	}
	for rows.Next() {
		perm := &Permission{}
		if err := rows.Scan(&perm.Code, &perm.Name, &perm.Mask); err != nil {
			log.Error(err)
		} else {
			perms = append(perms, perm)
		}
	}
	return perms, nil
}

// selectPermissionByCode возвращает объект привелегии по коду
func (db *database) selectPermissionByCode(code string) (*Permission, error) {
	perm := &Permission{}
	row := db.db.QueryRow(`SELECT code, name, mask FROM auth.permissions WHERE code = $1`, code)
	if err := row.Scan(&perm.Code, &perm.Name, &perm.Mask); err != nil {
		log.Error(err)
		log.Info(code)
		return nil, err
	}
	return perm, nil
}
