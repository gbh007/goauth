package goauth

import (
	"database/sql"
	"fmt"
)

// User структура для отображения данных пользователя
type User struct {
	ID        int
	Login     string
	Pass      string
	Salt      string
	Name      sql.NullString
	IsService bool
	CanAuth   bool
	PermMask  int
}

func (u User) String() string { return fmt.Sprintf("%d %s", u.ID, u.Login) }

// selectUsers возвращает список пользователей
func (db *database) selectUsers() ([]*User, error) {
	users := make([]*User, 0)
	rows, err := db.db.Query(`SELECT id, login, pass, salt, name, is_service, can_auth, perm_mask FROM auth.users`)
	if err != nil {
		log.Error(err)
		return nil, err
	}
	for rows.Next() {
		user := &User{}
		if err := rows.Scan(
			&user.ID,
			&user.Login,
			&user.Pass,
			&user.Salt,
			&user.Name,
			&user.IsService,
			&user.CanAuth,
			&user.PermMask,
		); err != nil {
			log.Error(err)
		} else {
			users = append(users, user)
		}
	}
	return users, nil
}

// selectUserByID возвращает пользователя по ID
func (db *database) selectUserByID(id int) (*User, error) {
	user := &User{}
	row := db.db.QueryRow(`SELECT id, login, pass, salt, name, is_service, can_auth, perm_mask FROM auth.users WHERE id = $1`, id)
	err := row.Scan(
		&user.ID,
		&user.Login,
		&user.Pass,
		&user.Salt,
		&user.Name,
		&user.IsService,
		&user.CanAuth,
		&user.PermMask,
	)
	if err != nil {
		log.Error(err)
		log.Info(id)
		return nil, err
	}
	return user, nil
}

// selectUserByLogin возвращает пользователя по логину
func (db *database) selectUserByLogin(login string) (*User, error) {
	user := &User{}
	row := db.db.QueryRow(`SELECT id, login, pass, salt, name, is_service, can_auth, perm_mask FROM auth.users WHERE login = $1`, login)
	err := row.Scan(
		&user.ID,
		&user.Login,
		&user.Pass,
		&user.Salt,
		&user.Name,
		&user.IsService,
		&user.CanAuth,
		&user.PermMask,
	)
	if err != nil {
		log.Error(err)
		log.Info(login)
		return nil, err
	}
	return user, nil
}

// insertUser добавляет новго пользователя
func (db *database) insertUser(user *User) (int, error) {
	row := db.db.QueryRow(`INSERT INTO auth.users (login, pass, salt, name, is_service, can_auth, perm_mask) VALUES ($1, $2, $3, $4, $5, $6, $7) RETURNING id`,
		user.Login,
		user.Pass,
		user.Salt,
		user.Name,
		user.IsService,
		user.CanAuth,
		user.PermMask,
	)
	err := row.Scan(&user.ID)
	if err != nil {
		log.Error(err)
		log.Info(user)
		return 0, err
	}
	return user.ID, nil
}

// updateUser обновляет данные пользователя
func (db *database) updateUser(user *User) error {
	_, err := db.db.Exec(`UPDATE auth.users SET login = $1, pass = $2, salt = $3, name = $4, is_service = $5, can_auth = $6, perm_mask = $7 WHERE id = $8`,
		user.Login,
		user.Pass,
		user.Salt,
		user.Name,
		user.IsService,
		user.CanAuth,
		user.PermMask,
		user.ID,
	)
	if err != nil {
		log.Error(err)
		log.Info(user)
		return err
	}
	return nil
}
