-- удаление старых таблиц
DROP TABLE IF EXISTS auth.sessions;
DROP TABLE IF EXISTS auth.users;
DROP TABLE IF EXISTS auth.permissions;
DROP SCHEMA IF EXISTS auth;

-- создание схемы для работы с авторизацией
CREATE SCHEMA IF NOT EXISTS auth;

-- создание таблицы уровней доступа
CREATE TABLE auth.permissions (
    code TEXT PRIMARY KEY,
    name TEXT,
    mask INT NOT NULL
);

-- заполнение стандартными значениями
INSERT INTO auth.permissions (code, name, mask) VALUES ('user_min', 'минимальные права пользователя', 1);
INSERT INTO auth.permissions (code, name, mask) VALUES ('admin_full', 'доступ к администраторскому функционалу', 2);

-- создание таблицы пользователей
CREATE TABLE auth.users (
    id SERIAL PRIMARY KEY,
    login TEXT UNIQUE NOT NULL,
    pass TEXT NOT NULL,
    salt TEXT NOT NULL,
    name TEXT,
    is_service BOOLEAN NOT NULL DEFAULT FALSE,
    can_auth BOOLEAN NOT NULL DEFAULT TRUE,
    perm_mask INT
);

-- добавление администратора (admin, admin) с маской на 31 разрешительную привелегию 0b1111111111111111111111111111111
INSERT INTO auth.users (login, pass, salt, perm_mask) values (
    'admin',
    '03d48985e661675ff5c6d8cc7ada0618f1bacf5c3a488104cef7dba015193c49',
    '7d309efbdfc3c7d905449b8743826724c33a7ed90193d41ceb8a17cbad89f8ea',
    2147483647
);

-- создание таблицы сессий
CREATE TABLE auth.sessions (
    token TEXT PRIMARY KEY NOT NULL,
    user_id INT REFERENCES auth.users (id) ON UPDATE CASCADE,
    last_update TIMESTAMP DEFAULT NULL
);
